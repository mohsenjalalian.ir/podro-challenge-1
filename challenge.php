<?php

function odd($var)
{
    // returns whether the input integer is odd
    return $var & 1;
}

$fptr = fopen("output.txt", "w");

$stdin = fopen("php://stdin", "r");


fscanf($stdin, "%[^\n]", $ar_temp);

$ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));

$result = array_key_first(array_filter(array_count_values($ar), 'odd'));

print($result);

fwrite($fptr, $result . "\n");

fclose($stdin);
fclose($fptr);
